---
title: "Family and fertility in the USA"
author: "Peter Northup - Hertha Firnberg Schulen"
date: 23.11.2017
---

## Family support policies: things are grim
![](this-is-fine.jpg)

## How much does it cost to give birth?
- About 20% of the population has Medicaid: $0
- The best insurance will also pay for deliery: $0
- But most insurance requires you to first pay your "deductible", and then part of the remaining charges: $1000-6,000, possibly more
- 8% have no insurance: $30,000-50,000 -- yes, really!

## Now get back to work!
![Paid family leave compared](paid-leave-comparative.png){ .stretch height=50% }

## Not quite that bad, but still bad
- 60% of employees eligible for up to 4 months of **unpaid** leave
- 80% of full-time and 30% of part-time workers have some paid sick leave (there's no legal right to any)
- 13% of private workers have some employer-provided paid family leave (PFL)
- CA, NJ, RI have PFL programs (4-6 weeks); NY & DC will soon (8-12 weeks)

::: notes

* (Worked at a company with >50 employees for >1 year and >25 hours/week)
* low-paid = bottom 25%
* But only 5% of part-time workers, and 6% of low-paid workers

:::

## So how much leave do Americans take?
- Fathers: an average of 1 week (paid + unpaid)
- Mothers:
  * 25%: 2 weeks or less
  * Average: 10 weeks
  * 95%: 26 weeks or less

## What about childcare?
- How bad is it?
- [Really, really bad](https://twitter.com/Pinboard/status/918577378242322434)

## The worst, in fact
![Childcare costs compared](childcare-compared.png){ .stretch height=50% }

::: notes
Accredited, center-based childcare for a dual-earner family with two young children and with earnings at 150 percent of the average full-time worker’s wage would cost that family, on average, 29 percent of their take-home pay. A poor single parent earning 50 percent of the national average wage would have to spend 52 percent of her income for the same services.
:::

## So how does that affect fertility?
![Fertility rates compared](TFR.svg){ .stretch height=50% }

## Desired fertility?
![Desired fertility](desired-fertility.png){ .stretch height=50% }

## What else?
![OECD USA outcomes](USA-outcomes.png){ .stretch height=50% }

